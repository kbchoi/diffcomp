## ---------------------------
## Project       : diffcomp
## Script        : search.R
## Purpose       : To detect differential composition
## Author(s)     : Kwangbom Choi, Ph.D, Yang Chen, Ph.D.
## Email(s)      : kwangbom@gmail.com, ychenang@umich.edu
## Date Created  : 11/28/2018
##
## Revision History:
##   Date        Author  Note
##   01/30/2019  kbchoi  Created 'scan_all' and 'scan_next' based on Yang's initial test snippet
##   04/30/2019  kbchoi  Created 'negative_search'
##   08/07/2019  ychen   Fixed 'negative_search' and 'estimate_bic'
##
## ---------------------------


negative_search <- function(cnt1, cnt2, p_val_cutoff=0.01) {
  num_classes <- length(cnt1)
  obs <- rbind(cnt1, cnt2)
  y0 <- combn(num_classes, 2, function(x) get_loglikelihood_for_mle(obs, get_mle_under_constraints2(obs, constraint_indices = x)))
  S0 <- combn(num_classes, 2)[, which.max(y0)]
  #
  # Yang's edit (start)
  #
  pval_test_S0 <- composition_test2 (obs = obs, constraint_indices = S0)$p_val
  if(pval_test_S0 < p_val_cutoff){
    cat(sprintf("No pairs were identified as non-differential components."))
    cat(sprintf("Testing marginal differential components:"))
    result_marginal_composition <- sapply(1:num_classes, function(k) composition_test_algorithm2 (obs, k))
    colnames(result_marginal_composition) <- paste("composition", 1:num_classes)
    return(result_marginal_composition)
  }
  #
  # Yang's edit (end)
  #
  cat(sprintf(c("The following components were identified as non-differential components: %d", " %d "), S0))
  cat(sprintf("(log-likelihood: %.4f)\n", max(y0)))
  cat(sprintf("Partitioning...\n\n"))
  S <- NULL
  D <- NULL
  for (k in setdiff(1:num_classes, S0)) {
    constraint_indices <- c(S0, k)
    y <- composition_test2(obs, constraint_indices = constraint_indices)
    cat(sprintf("Testing component: %d\t", k))
    cat(sprintf("p-value: %e\n", y$p_val))
    if (y$p_val > p_val_cutoff) {
      S <- c(S, k)
    } else {  # Reject H0
      D <- c(D, k)
    }
  }
  p_val <- NULL
  S=sort(c(S0, S))  # Yang's edit
  for (k in D) {
    constraint_indices <- c(S, k)
    y <- composition_test2(rbind(cnt1, cnt2), constraint_indices = constraint_indices)
    p_val <- c(p_val, y$p_val)
  }
  # return(list(S=sort(c(S0, S)), D=D, p_val=p_val))
  return(list(S=S, D=D, p_val=p_val))  # Yang's edit
}


forward_search <- function(cnt1, cnt2, bic_cutoff=2) {
  obs <- rbind(cnt1, cnt2)
  bic <- c()
  prev_y <- NULL
  y <- scan_next(cnt1, cnt2, maxonly=TRUE)
  repeat {
    set1 <- y$comb[!is.na(y$comb)]
    set2 <- head(set1, -1)
    cur_bic <- estimate_bic(obs=obs, group1=set1, group2=set2)
    bic <- c(bic, cur_bic)
    if (cur_bic < bic_cutoff) {
      break
    }
    prev_y <- y
    y <- scan_next(cnt1, cnt2, include=na.exclude(y$comb), maxonly=TRUE)  # repeat this to add more components
  }
  return(list(bic=bic, sol=prev_y, sol_next=y))
}


estimate_bic <- function(obs, group1, group2) {
  #
  # General Rules:
  #   BIC < 2      : Weak evidence that Model 1 is superior to Model 2
  #   2 ≤ BIC ≤ 6  : Moderate evidence that Model 1 is superior
  #   6 < BIC ≤ 10 : Strong evidence that Model 1 is superior
  #   BIC > 10     : Very strong evidence that Model 1 is superior
  #
  m1 <- length(group1)  # m1 > m2 assumed
  m2 <- length(group2)
  mle1 <- get_mle_under_constraints(obs, constraint_type = 1, un_constraint_indices = group1)
  mle2 <- get_mle_under_constraints(obs, constraint_type = 1, un_constraint_indices = group2)
  logL1 <- dmultinom(obs[1, ], prob = mle1[1,], log = TRUE) +
    dmultinom(obs[2, ], prob = mle1[2,], log = TRUE)
  logL2 <- dmultinom(obs[1, ], prob = mle2[1,], log = TRUE) +
    dmultinom(obs[2, ], prob = mle2[2,], log = TRUE)
  # bic <- 2 * (logL1 - logL2) - (m1 - m2) * log(dim(obs)[2])  # Yang's edit
  bic <- 2 * (logL1 - logL2) - m1 * log(sum(obs[1, ])) + m2 * log(sum(obs[2, ]))
  return (bic)
}


scan_next <- function(count_A, count_B, include=NULL, maxonly=FALSE, minonly=FALSE) {
  num_classes <- length(count_A)
  if (is.null(include)) {
    comb_mat <- matrix(nrow=num_classes, ncol=num_classes)
    res <- combn(num_classes, 1, function(x) composition_test (obs = rbind(count_A, count_B), un_constraint_indices = x))
    row.names(res) <- c('p_val', 'df', 'test_statistics')
    comb_mat <- matrix(nrow=num_classes, ncol=choose(num_classes, 1))
    comb_mat[1,] <- combn(num_classes, 1)
  } else {
    remcomp <- setdiff(1:num_classes, include)
    remlen <- length(remcomp)
    xlen <- length(include) + 1
    comb_mat <- matrix(nrow=num_classes, ncol=remlen)
    res <- matrix(nrow=3, ncol=remlen)
    k <- 1
    res <- NULL
    for (r in remcomp) {
      x <- union(include, r)
      res <- cbind(res, composition_test (obs = rbind(count_A, count_B), un_constraint_indices = x))
      comb_mat[1:xlen,k] <- x
      k <- k + 1
    }
  }
  if (maxonly) {
    surv <- which.max(res[1,])
    return(list(res=res[,surv], comb=comb_mat[,surv]))
  } else if (minonly) {
    surv <- which.min(res[1,])
    return(list(res=res[,surv], comb=comb_mat[,surv]))
  } else {
    return(list(res=res, comb=comb_mat))
  }
}


scan_combn <- function (count_A, count_B, k, maxonly=FALSE, minonly=FALSE) {
  num_classes <- length(count_A)
  comb_mat <- matrix(nrow = num_classes, ncol = choose(num_classes, k))
  comb_mat[1:k, ] <- combn(num_classes, k)
  res <- combn(num_classes, k, function(x) composition_test(obs = rbind(count_A, count_B), un_constraint_indices = x))
  row.names(res) <- c("p_val", "df", "test_statistics")
  if (maxonly) {
    surv <- which.max(res[1, ])
    return(list(res = res[, surv], comb = comb_mat[, surv]))
  } else if (minonly) {
    surv <- which.min(res[1, ])
    return(list(res = res[, surv], comb = comb_mat[, surv]))
  } else {
    return(list(res = res, comb = comb_mat))
  }
}


#classIDs <- as.integer(names(count_A))
scan_all <- function(count_A, count_B, maxonly=TRUE) {
  num_classes <- length(count_A)
  res <- combn(num_classes, 1, function(x) composition_test (obs = rbind(count_A, count_B), un_constraint_indices = x))
  row.names(res) <- c('p_val', 'df', 'test_statistics')
  comb_mat <- matrix(nrow=num_classes, ncol=num_classes)
  comb_mat[1,] <- combn(num_classes, 1)
  for(k in 2:floor(num_classes/2)){
    comb_mat_new <- matrix(nrow=num_classes, ncol=choose(num_classes, k))
    comb_mat_new[1:k,] <- combn(num_classes, k)
    res_new <- combn(num_classes, k, function(x) composition_test (obs = rbind(count_A, count_B), un_constraint_indices = x))
    res <- cbind(res, res_new)
    comb_mat <- cbind(comb_mat, comb_mat_new)
  }
  max_pval <- res[1, which.max(res[1,])]$p_val
  surv <- which(res[1,] == max_pval)
  if (maxonly) {
    return(list(res=res[,surv], comb=comb_mat[,surv]))
  } else {
    return(list(res=res, comb=comb_mat))
  }
}
